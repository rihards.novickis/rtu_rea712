--------------------------------------------------------------------------------
--! @file circular_shift_left.vhd
--------------------------------------------------------------------------------

library ieee;
library rtu;
use ieee.std_logic_1164.all;

--! @brief Design a circular shift-left (overshifted bits are mapped around to
--!        the other side) circuit manually. For example:
--!
--!        "0100111100110011" shifted by 4 => "1111001100110100"
--!
entity circular_shift_left is
  port(
    input  : in  std_logic_vector(15 downto 0); --! vector to be shifted
    shift  : in  std_logic_vector(3 downto 0);  --! encoded shift value
    output : out std_logic_vector(15 downto 0)  --! shifted vector
  );
end entity;

architecture RTL of circular_shift_left is
begin
  -- TODO: YOUR CODE GOES HERE
end architecture;
