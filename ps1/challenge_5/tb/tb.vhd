library std;
library ieee;
library rtu;
library osvvm;
library rtu_test;
library vunit_lib;

context vunit_lib.vunit_context;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use rtu_test.procedures.all;


entity tb is
  generic(
    runner_cfg : string;
    RANDOMIZED_TEST_COUNT : natural := 100
  );
end entity;

architecture RTL of tb is
  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
  signal  shift         : std_logic_vector(3 downto 0);
  signal  input, output : std_logic_vector(15 downto 0);

begin
  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity rtu.circular_shift_left
  port map(
    input  => input,
    shift  => shift,
    output => output);

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
    variable random    : RandomPType;
    variable expected  : std_logic_vector(15 downto 0);
    variable input_var : std_logic_vector(15 downto 0);
    variable shift_var : std_logic_vector(3 downto 0);
    variable shift_idx : natural;
  begin
    test_runner_setup(runner, runner_cfg);
    random.InitSeed(random'instance_name);

    while test_suite loop
      if run("random_coverage") then
        for i in 0 to RANDOMIZED_TEST_COUNT-1 loop
          input_var := random.RandSlv(input_var'length);
          shift_var := random.RandSlv(shift_var'length);
          shift_idx := to_integer(unsigned(shift_var));
          if shift_idx = 0 then
            expected := input_var;
          else
            expected := input_var(15-shift_idx downto 0)
                      & input_var(15 downto 15-shift_idx+1);
          end if;

          input <= input_var;
          shift <= shift_var;
          wait for 10 ns;

          check_slv(output, expected, "Checking output shift value");
        end loop;
      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;
end architecture;
