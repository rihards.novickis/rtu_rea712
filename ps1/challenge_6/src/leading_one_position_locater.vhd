--------------------------------------------------------------------------------
--! @file leading_one_position_locater.vhd
--------------------------------------------------------------------------------

library ieee;
library rtu;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use rtu.functions.all;

--! @brief The circuit locates the leading '1' starting from MSB to LSB and
--!        outputs its location as an unsigned number. The circuit also
--!        provides a valid signal that indicates if any '1' is found at
--!        all.
--!
--! @param DATA_WIDTH number of bits used for i_data.
--!
--! EXAMPLES:
--!        "0000000000000001" should result in o_valid='1', o_position=0
--!        "1000000000000000" should result in o_valid='1', o_position=15
--!        "0000000000000000" should result in o_valid='0'
entity leading_one_position_locater is
  generic(
    DATA_WIDTH : natural := 16
  );
  port(
    i_data     : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    o_position : out unsigned(log2c(DATA_WIDTH)-1 downto 0);
    o_valid    : out std_logic
  );
end entity;

architecture RTL of leading_one_position_locater is
begin
  -- TODO: YOUR CODE GOES HERE
end architecture;
