library ieee;
library rtu;
library rom;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use rtu.functions.all;

entity pattern_from_memory is
  port(
    clk        : in  std_logic;
    i_denable  : in  std_logic;
    i_hsync    : in  std_logic;
    i_vsync    : in  std_logic;
    i_hcounter : in  unsigned;
    i_vcounter : in  unsigned;
    o_denable  : out std_logic;
    o_hsync    : out std_logic;
    o_vsync    : out std_logic;
    o_color_r  : out std_logic_vector;
    o_color_g  : out std_logic_vector;
    o_color_b  : out std_logic_vector
  );
end entity;

architecture RTL of pattern_from_memory is
 constant IMAGE_WIDTH       : natural := 605;
 constant IMAGE_HEIGHT      : natural := 800;
 constant MEMORY_WORD_COUNT : natural := IMAGE_WIDTH*IMAGE_HEIGHT;
 constant ADDRESS_WIDTH     : natural := log2c(MEMORY_WORD_COUNT);

 signal address     : std_logic_vector(ADDRESS_WIDTH-1 downto 0);
 signal imageData   : std_logic_vector(7 downto 0);
begin

  ROM_IMAGE: entity rom.rom 
  port map (
    address	 => address,
    clock	 => clk,
    q	     => imageData);

  -- reg-state logic
  -- <your code goes here>

  -- next-state logic
  -- <your code goes here>

  -- outputs
  -- <your code goes here>

end architecture;
