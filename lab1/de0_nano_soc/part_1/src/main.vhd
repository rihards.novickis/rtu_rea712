-- library / package part
library ieee;
library rtu;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- interface with the world
entity main is
  generic(
    CLK_FREQUENCY : natural := 50000000
  );
  port(
    clk  : in  std_logic;
    led  : out std_logic
  );
end entity;

-- implementation
architecture RTL of main is
  -- declarative part
  constant COUNTER_MAX_VALUE : natural := CLK_FREQUENCY/2-1;
 
  signal led_reg, led_next : std_logic := '0';
  signal counter_reg, counter_next : std_logic_vector(24 downto 0) := (others => '0');
  signal counter_sum : std_logic_vector(25 downto 0);
  signal counter_is_full : boolean;
begin
  -- instantiate addition component
  U0: entity rtu.ripple_carry_adder
  generic map(DATA_WIDTH => 25)
  port map(
    a => counter_reg,
    b => std_logic_vector(to_unsigned(1, 25)),
    q => counter_sum);

  -- reg-state logic (registers and flip flops)
  process
  begin
    wait until rising_edge(clk);
    led_reg     <= led_next;
    counter_reg <= counter_next;
  end process;

  -- next-state logic (combinatoric)
  led_next <= not led_reg when counter_is_full else
              led_reg;

  counter_next <= (others => '0') when counter_is_full else
               counter_sum(counter_next'range);

  counter_is_full <= unsigned(counter_reg) = COUNTER_MAX_VALUE;

  -- outputs (connections to the outputs)
  led <= led_reg;

end architecture;
