library ieee;
use ieee.std_logic_1164.all;

entity edge_detector is
  port(
    clk        : in  std_logic;
    i_signal   : in  std_logic;
    o_rising   : out std_logic;
    o_falling  : out std_logic
  );
end entity;

architecture RTL of edge_detector is
begin

  -- reg-state logic
  -- <your code goes here>

  -- next-state logic
  -- <your code goes here>

  -- outputs
  -- <your code goes here>
end architecture;
