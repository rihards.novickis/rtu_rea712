library ieee;
library rtu;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use rtu.data_types.all;

entity lis3dh_master is
  port(
    clk  : in std_logic;
    rst  : in std_logic;

    -- master control interface
    o_start  : out std_logic;
    o_rw     : out std_logic;
    o_addr   : out std_logic_vector(5 downto 0);
    o_data   : out std_logic_vector(7 downto 0);
    i_done   : in  std_logic;
    i_data   : in  std_logic_vector(7 downto 0);

    -- read data
    o_id     : out std_logic_vector(7 downto 0);
    o_accl_x : out std_logic_vector(15 downto 0);
    o_accl_y : out std_logic_vector(15 downto 0);
    o_accl_z : out std_logic_vector(15 downto 0);
    o_done   : out std_logic
  );
end entity;

architecture RTL of lis3dh_master is
  subtype lis3dh_addr is std_logic_vector(5 downto 0);

  function to_lis3dh_addr(addr: natural) return lis3dh_addr is
  begin
    return std_logic_vector(to_unsigned(addr, 6));
  end function;

  -- registers for the LIS3DH MEMS digital motion sensor
  constant ADDR_WHO_AM_I     : lis3dh_addr := to_lis3dh_addr(16#0f#);
  constant ADDR_TEMP_CFG_REG : lis3dh_addr := to_lis3dh_addr(16#1f#);
  constant ADDR_CTRL_REG1    : lis3dh_addr := to_lis3dh_addr(16#20#);
  constant ADDR_CTRL_REG2    : lis3dh_addr := to_lis3dh_addr(16#21#);
  constant ADDR_CTRL_REG3    : lis3dh_addr := to_lis3dh_addr(16#22#);
  constant ADDR_CTRL_REG4    : lis3dh_addr := to_lis3dh_addr(16#23#);
  constant ADDR_CTRL_REG5    : lis3dh_addr := to_lis3dh_addr(16#24#);
  constant ADDR_CTRL_REG6    : lis3dh_addr := to_lis3dh_addr(16#25#);
  constant ADDR_OUT_X_L      : lis3dh_addr := to_lis3dh_addr(16#28#);
  constant ADDR_OUT_X_H      : lis3dh_addr := to_lis3dh_addr(16#29#);
  constant ADDR_OUT_Y_L      : lis3dh_addr := to_lis3dh_addr(16#2a#);
  constant ADDR_OUT_Y_H      : lis3dh_addr := to_lis3dh_addr(16#2b#);
  constant ADDR_OUT_Z_L      : lis3dh_addr := to_lis3dh_addr(16#2c#);
  constant ADDR_OUT_Z_H      : lis3dh_addr := to_lis3dh_addr(16#2d#);

  -- command bit decoding
  constant RW_CMD_WRITE : std_logic := '0';
  constant RW_CMD_READ  : std_logic := '1';

  -- states for the Finite State Machine
  type state_t is (
   s_idle,           -- first state to enter on reset
   s_read_who_am_i,  -- read device ID register (to test SPI)
   s_init_ctrl_reg1, -- control, data rate and X/Y/Z axis enable
   s_init_ctrl_reg2, -- control, on-chip data filtering
   s_init_ctrl_reg3, -- control, interrupts
   s_init_ctrl_reg4, -- control, data-rate , sensitivity, format, interface
   s_init_ctrl_reg5, -- control, reset, FIFO, force interrupts
   s_init_ctrl_reg6, -- control, click detection interrupts
   s_init_temp_cfg_reg, -- temperature and ADCs
   s_read_out_x_l, s_read_out_x_h,  -- X axis reading
   s_read_out_y_l, s_read_out_y_h,  -- Y axis reading
   s_read_out_z_l, s_read_out_z_h); -- Z axis reading

  signal state_reg, state_next : state_t := s_idle;
  signal id_en : std_logic;
  signal id_reg, id_next : std_logic_vector(7 downto 0) := (others => '0');
  signal x_low_en,   x_high_en   : std_logic;
  signal x_low_reg,  x_low_next  : std_logic_vector(7 downto 0) := (others => '0');
  signal x_high_reg, x_high_next : std_logic_vector(7 downto 0) := (others => '0');
  signal y_low_en,   y_high_en   : std_logic;
  signal y_low_reg,  y_low_next  : std_logic_vector(7 downto 0) := (others => '0');
  signal y_high_reg, y_high_next : std_logic_vector(7 downto 0) := (others => '0');
  signal z_low_en,   z_high_en   : std_logic;
  signal z_low_reg,  z_low_next  : std_logic_vector(7 downto 0) := (others => '0');
  signal z_high_reg, z_high_next : std_logic_vector(7 downto 0) := (others => '0');
  signal done_reg, done_next : std_logic := '0';
begin

  -- reg-state logic
  process(clk, rst)
  begin
    if rst = '1' then
      state_reg  <= s_idle;
      id_reg     <= (others => '0');
      x_low_reg  <= (others => '0');
      x_high_reg <= (others => '0');
      y_low_reg  <= (others => '0');
      y_high_reg <= (others => '0');
      z_low_reg  <= (others => '0');
      z_high_reg <= (others => '0');
      done_reg   <= '0';
    elsif rising_edge(clk) then
      state_reg <= state_next;
      if id_en = '1' then
        id_reg    <= id_next;
      end if;
      if x_low_en = '1' then
        x_low_reg <= x_low_next;
      end if;
      if x_high_en = '1' then
        x_high_reg <= x_high_next;
      end if;
      if y_low_en = '1' then
        y_low_reg <= y_low_next;
      end if;
      if y_high_en = '1' then
        y_high_reg <= y_high_next;
      end if;
      if z_low_en = '1' then
        z_low_reg <= z_low_next;
      end if;
      if z_high_en = '1' then
        z_high_reg <= z_high_next;
      end if;
      done_reg <= done_next;
    end if;
  end process;

  -- next-state logic
  process(all)
  begin
    -- default
    o_start     <= '0';
    o_rw        <= RW_CMD_READ;
    o_addr      <= (others => '0');
    o_data      <= (others => '0');
    state_next  <= state_reg;
    id_en       <= '0';
    x_low_en    <= '0';
    x_high_en   <= '0';
    y_low_en    <= '0';
    y_high_en   <= '0';
    z_low_en    <= '0';
    z_high_en   <= '0';
    id_next     <= i_data;
    x_low_next  <= i_data;
    x_high_next <= i_data;
    y_low_next  <= i_data;
    y_high_next <= i_data;
    z_low_next  <= i_data;
    z_high_next <= i_data;
    done_next   <= '0';

    -- state machine
    case state_reg is
      when s_idle =>
        -- TODO: go to new 's_read_who_am_i' state
        -- TODO: initiate new read command using 'o_start', 'o_rw' and 'o_addr' signals

      when s_read_who_am_i =>
        -- TODO: upon receiving 'i_done', enable 'id_reg' register using 'id_en'
        -- TODO: upon receiving 'i_done', go to new 's_init_ctrl_reg1' state
        -- TODO: upon receiving 'i_done', initiate new write command using
        --        'o_start', 'o_rw', 'o_addr' and 'o_data' signals, the
        --       register should be initialized with "01000111" value, i.e.
        --       set accelerometer data rate and enable all axis, see LIS3DH
        --       datasheet for reference.

      when s_init_ctrl_reg1 =>
        -- TODO: upon receiving 'i_done', go to new 's_init_ctrl_reg2' state
        -- TODO: upon receiving 'i_done', initiate new write command using
        --        'o_start', 'o_rw', 'o_addr' and 'o_data' signals, the
        --       register should be initialized with default "00000000" value.
        --       Note that for the purposes of the lab state could be omitted.

      when s_init_ctrl_reg2 =>
        -- TODO: upon receiving 'i_done', go to new 's_init_ctrl_reg3' state
        -- TODO: upon receiving 'i_done', initiate new write command using
        --        'o_start', 'o_rw', 'o_addr' and 'o_data' signals, the
        --       register should be initialized with default "00000000" value.
        --       Note that for the purposes of the lab state could be omitted.

      when s_init_ctrl_reg3 =>
        -- TODO: upon receiving 'i_done', go to new 's_init_ctrl_reg4' state
        -- TODO: upon receiving 'i_done', initiate new write command using
        --        'o_start', 'o_rw', 'o_addr' and 'o_data' signals, the
        --       register should be initialized with default "00001000" value,
        --       i.e. enable high-resolution mode.

      when s_init_ctrl_reg4 =>
        -- TODO: upon receiving 'i_done', go to new 's_init_ctrl_reg5' state
        -- TODO: upon receiving 'i_done', initiate new write command using
        --        'o_start', 'o_rw', 'o_addr' and 'o_data' signals, the
        --       register should be initialized with default "00000000" value.
        --       Note that for the purposes of the lab state could be omitted.

      when s_init_ctrl_reg5 =>
        -- TODO: upon receiving 'i_done', go to new 's_init_ctrl_reg6' state
        -- TODO: upon receiving 'i_done', initiate new write command using
        --        'o_start', 'o_rw', 'o_addr' and 'o_data' signals, the
        --       register should be initialized with default "00000000" value.
        --       Note that for the purposes of the lab state could be omitted.

      when s_init_ctrl_reg6 =>
        -- TODO: upon receiving 'i_done', go to new 's_init_temp_cfg_reg' state
        -- TODO: upon receiving 'i_done', initiate new write command using
        --        'o_start', 'o_rw', 'o_addr' and 'o_data' signals, the
        --       register should be initialized with default "00000000" value.
        --       Note that for the purposes of the lab state could be omitted.

      when s_init_temp_cfg_reg =>
        -- TODO: upon receiving 'i_done', go to new 's_read_out_x_l' state
        -- TODO: upon receiving 'i_done', initiate new read command using
        --        'o_start', 'o_rw' and 'o_addr' signals to read the lower half
        --       of the x accelerometer reading.

      when s_read_out_x_l =>
        -- TODO: upon receiving 'i_done', enable 'x_low_reg' register (to save
        --       input data from the spi_master)
        -- TODO: upon receiving 'i_done', go to new 's_read_out_x_h' state
        -- TODO: upon receiving 'i_done', initiate new read command using
        --        'o_start', 'o_rw' and 'o_addr' signals to read the higher half
        --       of the x accelerometer reading.

      when s_read_out_x_h =>
        -- TODO: upon receiving 'i_done', enable 'x_high_reg' register (to save
        --       input data from the spi_master)
        -- TODO: upon receiving 'i_done', go to new 's_read_out_y_l' state
        -- TODO: upon receiving 'i_done', initiate new read command using
        --        'o_start', 'o_rw' and 'o_addr' signals to read the lower half
        --       of the y accelerometer reading.

      when s_read_out_y_l =>
        -- TODO: upon receiving 'i_done', enable 'y_low_reg' register (to save
        --       input data from the spi_master)
        -- TODO: upon receiving 'i_done', go to new 's_read_out_y_h' state
        -- TODO: upon receiving 'i_done', initiate new read command using
        --        'o_start', 'o_rw' and 'o_addr' signals to read the higher half
        --       of the y accelerometer reading.

      when s_read_out_y_h =>
        -- TODO: upon receiving 'i_done', enable 'y_high_reg' register (to save
        --       input data from the spi_master)
        -- TODO: upon receiving 'i_done', go to new 's_read_out_z_l' state
        -- TODO: upon receiving 'i_done', initiate new read command using
        --        'o_start', 'o_rw' and 'o_addr' signals to read the lower half
        --       of the z accelerometer reading.

      when s_read_out_z_l =>
        -- TODO: upon receiving 'i_done', enable 'z_low_reg' register (to save
        --       input data from the spi_master)
        -- TODO: upon receiving 'i_done', go to new 's_read_out_z_h' state
        -- TODO: upon receiving 'i_done', initiate new read command using
        --        'o_start', 'o_rw' and 'o_addr' signals to read the higher half
        --       of the z accelerometer reading.

      when s_read_out_z_h =>
        -- TODO: upon receiving 'i_done', enable 'z_high_reg' register (to save
        --       input data from the spi_master)
        -- TODO: upon receiving 'i_done', go to previous 's_read_out_x_l' state
        -- TODO: upon receiving 'i_done', initiate previous read command using
        --        'o_start', 'o_rw' and 'o_addr' signals to read the lower half
        --       of the x accelerometer reading.
        -- TODO: generate output 'o_done' (using 'done_next') pulse for the
        -- further processing logic
    end case;
  end process;

  -- outputs
  -- TODO: connect 'o_id' output to the respective register output
  -- TODO: connect 'o_accl_x' output to the respective lower and higher half registers
  -- TODO: connect 'o_accl_y' output to the respective lower and higher half registers
  -- TODO: connect 'o_accl_z' output to the respective lower and higher half registers
  -- TODO: connect 'o_done' output to the respective register output

end architecture;
