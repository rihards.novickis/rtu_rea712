library ieee;
library rtu;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use rtu.functions.all;
use rtu.data_types.all;

entity sync_generator is
  generic(
    VGA_CONFIG : vga_config_t
  );
  port(
    clk        : in  std_logic;
    -- input counters
    i_hcounter : in  unsigned(
      log2c(VGA_CONFIG.HOR_DISPLAY+VGA_CONFIG.HOR_FRONT_PORCH+
      VGA_CONFIG.HOR_SYNC+VGA_CONFIG.HOR_BACK_PORCH)-1 downto 0);
    i_vcounter : in  unsigned(
      log2c(VGA_CONFIG.VER_DISPLAY+VGA_CONFIG.VER_FRONT_PORCH+
      VGA_CONFIG.VER_SYNC+VGA_CONFIG.VER_BACK_PORCH)-1 downto 0);
    -- control signals
    o_denable  : out std_logic;
    o_hsync    : out std_logic;
    o_vsync    : out std_logic;
    -- delayed counters
    o_hcounter : out unsigned(
      log2c(VGA_CONFIG.HOR_DISPLAY+VGA_CONFIG.HOR_FRONT_PORCH+
      VGA_CONFIG.HOR_SYNC+VGA_CONFIG.HOR_BACK_PORCH)-1 downto 0);
    o_vcounter : out unsigned(
      log2c(VGA_CONFIG.VER_DISPLAY+VGA_CONFIG.VER_FRONT_PORCH+
      VGA_CONFIG.VER_SYNC+VGA_CONFIG.VER_BACK_PORCH)-1 downto 0)
  );
end entity;


architecture RTL of sync_generator is
  -- VGA sync generation constants
  constant HOR_SYNC_START : natural := 
    VGA_CONFIG.HOR_DISPLAY+VGA_CONFIG.HOR_FRONT_PORCH;
  constant HOR_SYNC_STOP  : natural := 
    HOR_SYNC_START+VGA_CONFIG.HOR_SYNC-1;
  constant VER_SYNC_START : natural := 
    VGA_CONFIG.VER_DISPLAY+VGA_CONFIG.VER_FRONT_PORCH;
  constant VER_SYNC_STOP  : natural := 
    VER_SYNC_START+VGA_CONFIG.VER_SYNC-1;

  -- Sync generation
  -- <your code goes here>
begin

  -- reg-state logic
  -- <your code goes here>
  
  -- next-state logic
  -- <your code goes here>
  
  -- outputs
  -- <your code goes here>
     
end architecture;
