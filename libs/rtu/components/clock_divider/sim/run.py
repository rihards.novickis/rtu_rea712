#!/usr/bin/env python3
from vunit import VUnit

# create VUnit project
prj = VUnit.from_argv()

# add OSVVM library
prj.add_osvvm()
prj.add_verification_components()

# add custom libraries
prj.add_library("rtu")
prj.add_library("rtu_test")

# add sources and testbenches
prj.library("rtu").add_source_file("../src/clock_divider.vhd")
prj.library("rtu").add_source_file("../tb/tb.vhd")
prj.library("rtu_test").add_source_file("../../../../rtu_test/pkg/procedures.vhd")

for i in range(1,12):
    prj.library("rtu").test_bench("tb").test("number_of_output_cycles").add_config(
        name="cascade_count=" + str(i),
        generics=dict(
          CASCADE_COUNT = i))


# run VUnit simulation
prj.main()
