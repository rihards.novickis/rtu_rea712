library std;
library ieee;
library rtu;
library osvvm;
library rtu_test;
library vunit_lib;

context vunit_lib.vunit_context;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use rtu_test.procedures.all;

entity tb is
  generic(
    runner_cfg    : string;
    CASCADE_COUNT : natural := 10
  );
end entity;

architecture RTL of tb is
  constant CLK_PERIOD : time := 10 ns;
  constant INPUT_CYCLE_COUNT : natural := 10*(2**CASCADE_COUNT);

  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
  signal i_clk : std_logic := '0';
  signal o_clk : std_logic := '0';

begin
  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity rtu.clock_divider
  generic map(CASCADE_COUNT => CASCADE_COUNT)
  port map(
    i_clk => i_clk,
    o_clk => o_clk
  );

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
    variable o_clk_old : std_logic := '0';
    variable output_cycle_count : natural := 0;

    procedure update_output_cycle_count is
    begin
      -- check if there was a rising edge for output clock
      if o_clk_old = '0' and o_clk = '1' then
        output_cycle_count := output_cycle_count + 1;
      end if;

      -- update old output clock state
      o_clk_old := o_clk;
    end procedure;
  begin
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
      if run("number_of_output_cycles") then
        for i in 0 to INPUT_CYCLE_COUNT-1 loop
          -- input clock's falling edge
          update_output_cycle_count;
          i_clk <= '0';
          wait for CLK_PERIOD/2;

          -- input clock's rising edge
          update_output_cycle_count;
          i_clk <= '1';
          wait for CLK_PERIOD/2;
        end loop;

        -- check if number of output clock cycles is as expected
        check(output_cycle_count=INPUT_CYCLE_COUNT/(2**CASCADE_COUNT),
          "Testing numer of output clock cycles, " 
          & "CASCADE_COUNT: "   & integer'image(CASCADE_COUNT) & "; "
          & "INPUT: "           & integer'image(INPUT_CYCLE_COUNT) & "; "
          & "EXPECTED OUTPUT: " & integer'image(INPUT_CYCLE_COUNT/(2**CASCADE_COUNT)) & "; "
          & "GOT OUTPUT: "      & integer'image(output_cycle_count));
      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;
end architecture;

