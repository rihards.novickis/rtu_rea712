library ieee;
library rtu;
use ieee.std_logic_1164.all;

--------------------------------------------------------------------------------
--! @brief Flip-flop based clock divider. The clock is divided by linking 
--!        previous flip-flop's output to the clock input of the next flip
--!        flop
--!
--! @param CASCADE_COUNT Number of flip-flops to use in the clock divider.
--!        The number N of flip-flops correspond to frequency division of
--!        2^N
entity clock_divider is
  generic(
    CASCADE_COUNT : natural := 10 
  );
  port(
    i_clk  : in  std_logic; --! input clock
    o_clk  : out std_logic  --! output clock, with frequency of (f/2^N)
  );
end entity;

architecture RTL of clock_divider is
  -- declarative part
  -- <your code goes here>
begin

  -- reg-state logic
  -- <your code goes here>

  -- next-state logic
  -- <your code goes here>
 
  -- outputs
  -- <your code goes here>

end architecture;
