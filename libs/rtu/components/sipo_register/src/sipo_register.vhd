library ieee;
library rtu;
use ieee.std_logic_1164.all;


entity sipo_register is
  port(
    clk      : in  std_logic;       --! clock
    rst      : in  std_logic;       --! asynchronous reset
    i_enable : in  std_logic;       --! enable shift register
    i_data   : in  std_logic;       --! input (serial) data, single bit
    o_data   : out std_logic_vector --! output (parallel) data
  );
end entity;


architecture RTL of sipo_register is
  constant DATA_WIDTH : natural := o_data'length;
  signal data_reg, data_next : std_logic_vector(DATA_WIDTH-1 downto 0) := (others =>'0');
begin

  -- reg-state logic
  -- <TODO: your code goes here>

  -- next-state logic
  -- <TODO: your code goes here>

  -- outputs
  -- <TODO: your code goes here>
end architecture;
