library ieee;
use ieee.std_logic_1164.all;

entity button_debounce is
  port(
    clk      : in  std_logic;
    i_en     : in  std_logic;
    i_signal : in  std_logic;
    o_signal : out std_logic
  );
end entity;

architecture RTL of button_debounce is
  constant CASCADE_COUNT : natural := 3;
begin
  -- reg-state logic
  -- <your code goes here>

  -- next-state logic
  -- <your code goes here>

  -- outputs
  -- <your code goes here>

end architecture;
