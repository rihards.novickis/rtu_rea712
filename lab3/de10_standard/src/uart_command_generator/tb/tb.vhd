library std;
library ieee;
library rtu;
library lab3;
library osvvm;
library rtu_test;
library vunit_lib;

context vunit_lib.vunit_context;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use rtu.data_types.all;
use lab3.functions.all;

entity tb is
  generic(
    runner_cfg : string
  );
end entity;

architecture RTL of tb is
  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------
  constant CLK_PERIOD : time := 10 ns;

  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
  signal clk                  : std_logic := '0';
  signal i_btn0_rising_event  : std_logic := '0';
  signal i_btn0_falling_event : std_logic := '0';
  signal i_btn1_rising_event  : std_logic := '0';
  signal i_btn1_falling_event : std_logic := '0';
  signal i_busy               : std_logic := '0'; 
  signal o_valid              : std_logic := '0';
  signal o_data               : std_logic_vector(7 downto 0) := (others => '0');

  -----------------------------------------------------------------------------
  -- DUT internal signals
  -----------------------------------------------------------------------------
  type state_hl_t is (s_idle, s_btn0_prepare, s_sending_leds, s_sending_hex0, s_sending_hex1);
  type state_ll_t is (s_idle, s_sending_cmd, s_sending_addr, s_sending_data);
  signal dut_state_hl_reg : state_hl_t;
  signal dut_state_ll_reg : state_ll_t;

begin
  -----------------------------------------------------------------------------
  -- clk
  -----------------------------------------------------------------------------
  clk <= not clk after CLK_PERIOD/2;

  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity lab3.uart_command_generator
  port map(
    clk                  => clk,
    i_btn0_rising_event  => i_btn0_rising_event,
    i_btn0_falling_event => i_btn0_falling_event,
    i_btn1_rising_event  => i_btn1_rising_event,
    i_btn1_falling_event => i_btn1_falling_event,
    i_busy               => i_busy,
    o_valid              => o_valid,
    o_data               => o_data);

  -----------------------------------------------------------------------------
  -- DUT internal signals
  -----------------------------------------------------------------------------
  dut_state_hl_reg <= << signal .tb.DUT.state_hl_reg : state_hl_t >>;
  dut_state_ll_reg <= << signal .tb.DUT.state_ll_reg : state_ll_t >>;

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process
    --variable dut_addr : std_logic_vector(5 downto 0);
    --variable dut_data : std_logic_vector(7 downto 0);
    --variable dut_cmd  : std_logic_vector(7 downto 0);

    procedure sim_btn0_rising_event is
    begin
      info("Simulating btn0 rising event");
      wait until rising_edge(clk);
      i_btn0_rising_event <= '1';
      wait until rising_edge(clk);
      i_btn0_rising_event <= '0';
    end procedure;

    procedure sim_btn1_rising_event is
    begin
      info("Simulating btn1 rising event");
      wait until rising_edge(clk);
      i_btn1_rising_event <= '1';
      wait until rising_edge(clk);
      i_btn1_rising_event <= '0';
    end procedure;

    procedure sim_and_check_uart_packet(constant v_data : in integer) is
    begin
      wait until rising_edge(o_valid) for 500*CLK_PERIOD;
      check(o_valid = '1',             "The expected 'o_valid' high pulse has not been generated");
      check(unsigned(o_data) = v_data, "Received incorrect data, EXPECTED: "
                                        & integer'image(v_data) & "; GOT: "
                                        & integer'image(to_integer(unsigned(o_data))));

      info("Simulating busy");
      i_busy <= '1';
      wait for 5*CLK_PERIOD;

      info("Simulating completion");
      wait until rising_edge(clk);
      i_busy <= '0';

    end procedure;

    procedure sim_and_check_uart_packet(constant v_data : in std_logic_vector) is
    begin
      sim_and_check_uart_packet(to_integer(unsigned(v_data)));
    end procedure;


  begin
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
      if run("idle") then
        check(dut_state_hl_reg = s_idle, "High-level state by default must be idle");
        check(dut_state_ll_reg = s_idle, "Low-level state by default must be idle");


      elsif run("button0_hl_states") then

        sim_btn0_rising_event;

        wait on dut_state_hl_reg for 5*CLK_PERIOD;
        check(dut_state_hl_reg = s_btn0_prepare, "The expected 's_btn0_prepare' state not reached");

        wait on dut_state_hl_reg for 500*CLK_PERIOD;
        check(dut_state_hl_reg = s_sending_leds, "The expected 's_sending_leds' state not reached");

        wait on dut_state_hl_reg for 500*CLK_PERIOD;
        check(dut_state_hl_reg = s_sending_hex0, "The expected 's_sending_hex0' state not reached");

        wait on dut_state_hl_reg for 500*CLK_PERIOD;
        check(dut_state_hl_reg = s_sending_hex1, "The expected 's_sending_hex1' state not reached");

        wait on dut_state_hl_reg for 500*CLK_PERIOD;
        check(dut_state_hl_reg = s_idle, "The expected 's_idle' state not reached");


      elsif run("button0_transfers") then
        sim_btn0_rising_event;

        info("Expecting UART command packet request (LEDS)");
        sim_and_check_uart_packet(87); -- "Write command character"

        info("Expecting UART address packet request (LEDS)");
        sim_and_check_uart_packet(0);  -- "Address"

        info("Expecting UART data packet request (LEDS)");
        sim_and_check_uart_packet(1);  -- "Data"


        info("Expecting UART command packet request (HEX0)");
        sim_and_check_uart_packet(87); -- "Write command character"

        info("Expecting UART address packet request (HEX0)");
        sim_and_check_uart_packet(1);  -- "Address"

        info("Expecting UART data packet request (HEX0)");
        sim_and_check_uart_packet(dec2segment(0));  -- "Data"


        info("Expecting UART command packet request (HEX1)");
        sim_and_check_uart_packet(87); -- "Write command character"

        info("Expecting UART address packet request (HEX1)");
        sim_and_check_uart_packet(2);  -- "Address"

        info("Expecting UART data packet request (HEX1)");
        sim_and_check_uart_packet(dec2segment(1));  -- "Data"


        wait for 5*CLK_PERIOD;
        sim_btn0_rising_event;

        info("Expecting UART command packet request (LEDS)");
        sim_and_check_uart_packet(87); -- "Write command character"

        info("Expecting UART address packet request (LEDS)");
        sim_and_check_uart_packet(0);  -- "Address"

        info("Expecting UART data packet request (LEDS)");
        sim_and_check_uart_packet(2);  -- "Data"


        info("Expecting UART command packet request (HEX0)");
        sim_and_check_uart_packet(87); -- "Write command character"

        info("Expecting UART address packet request (HEX0)");
        sim_and_check_uart_packet(1);  -- "Address"

        info("Expecting UART data packet request (HEX0)");
        sim_and_check_uart_packet(dec2segment(0));  -- "Data"


        info("Expecting UART command packet request (HEX1)");
        sim_and_check_uart_packet(87); -- "Write command character"

        info("Expecting UART address packet request (HEX1)");
        sim_and_check_uart_packet(2);  -- "Address"

        info("Expecting UART data packet request (HEX1)");
        sim_and_check_uart_packet(dec2segment(2));  -- "Data"


      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;
end architecture;
