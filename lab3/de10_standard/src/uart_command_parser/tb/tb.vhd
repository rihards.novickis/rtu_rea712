library std;
library ieee;
library rtu;
library lab3;
library osvvm;
library rtu_test;
library vunit_lib;

context vunit_lib.vunit_context;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;
use osvvm.RandomPkg.all;
use vunit_lib.com_pkg.all;
use rtu.data_types.all;
use lab3.functions.all;

entity tb is
  generic(
    runner_cfg : string;
    REGISTER_ADDR_WIDTH : natural := 4;
    REGISTER_DATA_WIDTH : natural := 8
  );
end entity;

architecture RTL of tb is
  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------
  constant CLK_PERIOD : time := 10 ns;

  -----------------------------------------------------------------------------
  -- DUT interfacing
  -----------------------------------------------------------------------------
  signal clk      : std_logic := '0';
  signal i_valid  : std_logic := '0';
  signal i_data   : std_logic_vector(7 downto 0) := (others => '0');
  signal o_wreq   : std_logic := '0';
  signal o_waddr  : std_logic_vector(REGISTER_ADDR_WIDTH-1 downto 0) := (others => '0');
  signal o_wdata  : std_logic_vector(REGISTER_DATA_WIDTH-1 downto 0) := (others => '0');
  signal o_error  : std_logic := '0';

  -----------------------------------------------------------------------------
  -- DUT internal signals
  -----------------------------------------------------------------------------
  type state_t is (s_idle, s_recv_wcmd, s_recv_waddr, s_recv_wdata, s_wrequest, s_error);
  signal dut_state_reg : state_t;

begin
  -----------------------------------------------------------------------------
  -- clk
  -----------------------------------------------------------------------------
  clk <= not clk after CLK_PERIOD/2;

  -----------------------------------------------------------------------------
  -- DUT instantation
  -----------------------------------------------------------------------------
  DUT: entity lab3.uart_command_parser
  generic map(
    REGISTER_ADDR_WIDTH => REGISTER_ADDR_WIDTH,
    REGISTER_DATA_WIDTH => REGISTER_DATA_WIDTH)
  port map(
    clk      => clk,
    i_valid  => i_valid,
    i_data   => i_data,
    o_wreq   => o_wreq,
    o_waddr  => o_waddr,
    o_wdata  => o_wdata,
    o_error  => o_error);

  -----------------------------------------------------------------------------
  -- DUT internal signals
  -----------------------------------------------------------------------------
  dut_state_reg <= << signal .tb.DUT.state_reg : state_t >>;

  -----------------------------------------------------------------------------
  -- Test sequencer
  -----------------------------------------------------------------------------
  process

    procedure dut_send_data(data : std_logic_vector(7 downto 0)) is
    begin
      info("Sending data: " & to_hstring(data));
      wait until rising_edge(clk);
      i_valid <= '1';
      i_data  <= data;

      wait until rising_edge(clk);
      i_valid <= '0';
      i_data  <= (others => 'X');
    end procedure;

    procedure dut_check_state(state : state_t) is
    begin
      wait until dut_state_reg = state for 10*CLK_PERIOD;
      check(dut_state_reg = state, "State mismatch, "
        & "EXPECTED: " & state_t'image(state) & "; "
        & "GOT: "      & state_t'image(dut_state_reg));
    end procedure;

    procedure dut_check_write_request(waddr,wdata : std_logic_vector) is
    begin
      wait until rising_edge(o_wreq) for 10*CLK_PERIOD;
      check(o_wreq = '1', "Write request high state not reached (timeout)");

      check(o_waddr = waddr, "Write address seems wrong, "
        & "EXPECTED: " & to_hstring(waddr) & "; "
        & "GOT: "      & to_hstring(o_waddr));

      check(o_wdata = wdata, "Write data seems wrong, "
        & "EXPECTED: " & to_hstring(wdata) & "; "
        & "GOT: "      & to_hstring(o_wdata));

      check(o_error = '0', "Error is expected to be low");
    end procedure;

  begin
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
      if run("write_addr_full_coverage") then
        check(dut_state_reg = s_idle, "Initial state by default must be idle");

        for a in 0 to 2**REGISTER_ADDR_WIDTH-1 loop
          -- send command
          dut_send_data(std_logic_vector(to_unsigned(87,8)));
          dut_check_state(s_recv_wcmd);

          -- send address
          dut_send_data(std_logic_vector(to_unsigned(a,8)));
          dut_check_state(s_recv_waddr);

          -- send data
          dut_send_data("10101010");
          dut_check_state(s_recv_wdata);

          -- checking write request generation
         dut_check_write_request(
           std_logic_vector(to_unsigned(a,REGISTER_ADDR_WIDTH)),
           "10101010");

         wait for 2*CLK_PERIOD;
        end loop;

      elsif run("write_data_full_coverage") then
        check(dut_state_reg = s_idle, "Initial state by default must be idle");

        for d in 0 to 2**REGISTER_DATA_WIDTH-1 loop
          -- send command
          dut_send_data(std_logic_vector(to_unsigned(87,8)));
          dut_check_state(s_recv_wcmd);

          -- send address
          dut_send_data(std_logic_vector(to_unsigned(2**REGISTER_ADDR_WIDTH-1,8)));
          dut_check_state(s_recv_waddr);

          -- send data
          dut_send_data(std_logic_vector(to_unsigned(d,8)));
          dut_check_state(s_recv_wdata);

          -- checking write request generation
         dut_check_write_request(
           std_logic_vector(to_unsigned(2**REGISTER_ADDR_WIDTH-1,REGISTER_ADDR_WIDTH)),
           std_logic_vector(to_unsigned(d,REGISTER_DATA_WIDTH)));

         wait for 2*CLK_PERIOD;
        end loop;

      elsif run("error_state") then
        check(dut_state_reg = s_idle, "Initial state by default must be idle");
        for c in 0 to 255 loop
          -- omit valid commands
          if c = 82 or c = 87 then
            next;
          end if;

          -- send invalid command
          dut_send_data(std_logic_vector(to_unsigned(c,8)));
          dut_check_state(s_error);

          -- send reset command
          dut_send_data(std_logic_vector(to_unsigned(82,8)));
          dut_check_state(s_idle);
        end loop;
      end if;

    end loop;

    test_runner_cleanup(runner);
  end process;
end architecture;

