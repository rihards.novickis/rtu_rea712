library ieee;
library rtu;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use rtu.functions.all;

entity pulse_generator is
  generic(
    COUNTER_MAX_VALUE : natural := 49_999
  );
  port(
    clk     : in  std_logic;
    o_pulse : out std_logic
  );
end entity;


architecture RTL of pulse_generator is
  signal pulse_reg, pulse_next : std_logic := '0';
  signal counter_reg, counter_next : unsigned(log2c(COUNTER_MAX_VALUE)-1 downto 0)
    := (others => '0');
begin
  -- reg-state logic
  process
  begin
    wait until rising_edge(clk);
    pulse_reg   <= pulse_next;
    counter_reg <= counter_next;
  end process;

  -- next-state logic
  counter_next <= (others => '0') when counter_reg = COUNTER_MAX_VALUE else
                  counter_reg + 1;

  pulse_next <= '1' when counter_reg = COUNTER_MAX_VALUE else
                '0';

  -- outputs
  o_pulse <= pulse_reg;

end architecture;
