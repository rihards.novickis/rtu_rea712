--------------------------------------------------------------------------------
--! @file functions.vhd
--------------------------------------------------------------------------------

-- libraries and packages
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package functions is

  -- dec to 7-segment hex indicator conversion
  function dec2segment(input : integer)  return std_logic_vector;
  function dec2segment(input : unsigned) return std_logic_vector;

end package;

-- implementations of the package (functions, procedures)
package body functions is
  function dec2segment(input : integer) return std_logic_vector is
  begin

    -- TODO: create mapping
    case input is
      when 0 => return      "1111111";
      when 1 => return      "1111111";
      when 2 => return      "1111111";
      when 3 => return      "1111111";
      when 4 => return      "1111111";
      when 5 => return      "1111111";
      when 6 => return      "1111111";
      when 7 => return      "1111111";
      when 8 => return      "1111111";
      when 9 => return      "1111111";
      when 10 => return     "1111111";
      when 11 => return     "1111111";
      when 12 => return     "1111111";
      when 13 => return     "1111111";
      when 14 => return     "1111111";
      when others => return "1111111";
    end case;
  end function;

  function dec2segment(input : unsigned) return std_logic_vector is
  begin
    return dec2segment(to_integer(input));
  end function;
end package body;
